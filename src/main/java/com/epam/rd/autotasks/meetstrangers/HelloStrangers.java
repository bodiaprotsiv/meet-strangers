package com.epam.rd.autotasks.meetstrangers;

import java.io.IOException;
import java.util.Scanner;

public class HelloStrangers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Scanner lineScanner = new Scanner(System.in);
        int number = in.nextInt();
        if(number>0)
        {
            for(int i=0;i<number;i++)
            {
                String name = lineScanner.nextLine();
                System.out.println("Hello, " + name);
            }
        }
        else if(number==0)
        {
            System.out.println("Oh, it looks like there is no one here");
        }
        else
        {
            System.out.println("Seriously? Why so negative?");
        }
    }
}
